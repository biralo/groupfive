package library.payfine;
import library.entities.Library;
import library.entities.Member;

public class PayFineControl {
	
	private PayFineUI Ui;
<<<<<<< HEAD
	private enum ControlState { INITIALISED, READY, PAYING, COMPLETED, CANCELLED };
	private ControlState State;
	
	private Library Library;
	private Member Member;


	public PayFineControl() {
		this.Library = Library.getinstance();
		State = ControlState.INITIALISED;
	}
	
	
	public void setUI(PayFineUI UI) {
		if (!State.equals(ControlState.INITIALISED)) {
			throw new RuntimeException("PayFineControl: cannot call setUI except in INITIALISED state");
		}	
		this.Ui = UI;
		UI.setState(PayFineUI.UIState.READY);
		State = ControlState.READY;		
	}


	public void cardSwiped(int Member_Id) {
		if (!State.equals(ControlState.READY)) 
			throw new RuntimeException("PayFineControl: cannot call cardSwiped except in READY state");
			
		Member = Library.gEt_MeMbEr(Member_Id);
		
		if (Member == null) {
			Ui.Display("Invalid Member Id");
			return;
		}
		
		
		Ui.Display(Member.toString());
		Ui.setState(PayFineUI.UIState.PAYING);
		State = ControlState.PAYING;
	}
		
	public void CaNcEl() {
		Ui.setState(PayFineUI.UIState.CANCELLED);
		State = ControlState.CANCELLED;
=======
	private enum cOnTrOl_sTaTe { INITIALISED, READY, PAYING, COMPLETED, CANCELLED };
	private cOnTrOl_sTaTe StAtE;
	
	private Library LiBrArY;
	private Member MeMbEr;


	public pAY_fINE_cONTROL() {
		this.LiBrArY = Library.GeTiNsTaNcE();
		StAtE = cOnTrOl_sTaTe.INITIALISED;
	}
	
	
	public void SeT_uI(PayFineUI uI) {
		if (!StAtE.equals(cOnTrOl_sTaTe.INITIALISED)) {
			throw new RuntimeException("PayFineControl: cannot call setUI except in INITIALISED state");
		}	
		this.Ui = uI;
		uI.SeT_StAtE(PayFineUI.uI_sTaTe.READY);
		StAtE = cOnTrOl_sTaTe.READY;		
	}


	public void CaRd_sWiPeD(int MeMbEr_Id) {
		if (!StAtE.equals(cOnTrOl_sTaTe.READY)) 
			throw new RuntimeException("PayFineControl: cannot call cardSwiped except in READY state");
			
		MeMbEr = LiBrArY.gEt_MeMbEr(MeMbEr_Id);
		
		if (MeMbEr == null) {
			Ui.DiSplAY("Invalid Member Id");
			return;
		}
		Ui.DiSplAY(MeMbEr.toString());
		Ui.SeT_StAtE(PayFineUI.uI_sTaTe.PAYING);
		StAtE = cOnTrOl_sTaTe.PAYING;
	}
	
	
	public void CaNcEl() {
		Ui.SeT_StAtE(PayFineUI.uI_sTaTe.CANCELLED);
		StAtE = cOnTrOl_sTaTe.CANCELLED;
>>>>>>> d3df193f12cce49315b4ed1b47ff09806674263d
	}


	public double PaY_FiNe(double AmOuNt) {
<<<<<<< HEAD
		if (!State.equals(ControlState.PAYING)) 
			throw new RuntimeException("PayFineControl: cannot call payFine except in PAYING state");
			
		double change = Member.PaY_FiNe(AmOuNt);
		if (change > 0) 
			Ui.Display(String.format("Change: $%.2f", change));
		
		Ui.Display(Member.toString());
		Ui.setState(PayFineUI.UIState.COMPLETED);
		State = ControlState.COMPLETED;
		return change;
=======
		if (!StAtE.equals(cOnTrOl_sTaTe.PAYING)) 
			throw new RuntimeException("PayFineControl: cannot call payFine except in PAYING state");
			
		double ChAnGe = MeMbEr.PaY_FiNe(AmOuNt);
		if (ChAnGe > 0) 
			Ui.DiSplAY(String.format("Change: $%.2f", ChAnGe));
		
		Ui.DiSplAY(MeMbEr.toString());
		Ui.SeT_StAtE(PayFineUI.uI_sTaTe.COMPLETED);
		StAtE = cOnTrOl_sTaTe.COMPLETED;
		return ChAnGe;
>>>>>>> d3df193f12cce49315b4ed1b47ff09806674263d
	}
	


}
