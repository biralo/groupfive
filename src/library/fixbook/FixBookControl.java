package library.fixbook;
import library.entities.Book;
import library.entities.Library;
// Fix book Control
public class FixBookControl {
	
	private FixBookUI Ui;

	private enum ControlState { INITIALISED, READY, FIXING };
	private ControlState State;
	
	private Library Library;
	private Book currentBook;


	public FixBookControl() {
		this.Library = Library.getinstancea
		State = ControlState.INITIALISED;
	}
	
	
	public void SetUI(FixBookUI ui) {
		if (!State.equals(ControlState.INITIALISED)) 
			throw new RuntimeException("FixBookControl: cannot call setUI except in INITIALISED state");
			
		this.Ui = ui;
		ui.SetState(FixBookUI.UIState.READY);
		State = ControlState.READY;		
	}


	public void bookScanned(int BoOkId) {
		if (!State.equals(ControlState.READY)) 
			throw new RuntimeException("FixBookControl: cannot call bookScanned except in READY state");
			
		currentBook = Library.getBook(BoOkId);
		
		if (currentBook == null) {
			Ui.display("Invalid bookId");
			return;
		}
		if (!currentBook.iS_DaMaGeD()) {
			Ui.display("Book has not been damaged");
			return;
		}
		Ui.display(currentBook.toString());
		Ui.SetState(FixBookUI.UIState.FIXING);
		State = ControlState.FIXING;		
	}


	public void fixBook (boolean mustFix) {
		if (!State.equals(ControlState.FIXING)) 
			throw new RuntimeException("FixBookControl: cannot call fixBook except in FIXING state");
			
		if (mustFix) 
			Library.RePaIr_BoOk(currentBook);
		
		currentBook = null;
		Ui.SetState(FixBookUI.UIState.READY);
		State = ControlState.READY;		
	}

	
	public void ScanningComplete() {
		if (!State.equals(ControlState.READY)) 
			throw new RuntimeException("FixBookControl: cannot call scanningComplete except in READY state");
			
		Ui.SetState(FixBookUI.UIState.COMPLETED);		

	private enum CoNtRoL_StAtE { INITIALISED, READY, FIXING };
	private CoNtRoL_StAtE StAtE;
	
	private Library LiBrArY;
	private Book CuRrEnT_BoOk;


	public fIX_bOOK_cONTROL() {
		this.LiBrArY = Library.GeTiNsTaNcE();
		StAtE = CoNtRoL_StAtE.INITIALISED;
	}
	
	
	public void SeT_Ui(FixBookUI ui) {
		if (!StAtE.equals(CoNtRoL_StAtE.INITIALISED)) 
			throw new RuntimeException("FixBookControl: cannot call setUI except in INITIALISED state");
			
		this.Ui = ui;
		ui.SeT_StAtE(FixBookUI.uI_sTaTe.READY);
		StAtE = CoNtRoL_StAtE.READY;		
	}


	public void BoOk_ScAnNeD(int BoOkId) {
		if (!StAtE.equals(CoNtRoL_StAtE.READY)) 
			throw new RuntimeException("FixBookControl: cannot call bookScanned except in READY state");
			
		CuRrEnT_BoOk = LiBrArY.gEt_BoOk(BoOkId);
		
		if (CuRrEnT_BoOk == null) {
			Ui.dIsPlAy("Invalid bookId");
			return;
		}
		if (!CuRrEnT_BoOk.iS_DaMaGeD()) {
			Ui.dIsPlAy("Book has not been damaged");
			return;
		}
		Ui.dIsPlAy(CuRrEnT_BoOk.toString());
		Ui.SeT_StAtE(FixBookUI.uI_sTaTe.FIXING);
		StAtE = CoNtRoL_StAtE.FIXING;		
	}


	public void FiX_BoOk(boolean mUsT_FiX) {
		if (!StAtE.equals(CoNtRoL_StAtE.FIXING)) 
			throw new RuntimeException("FixBookControl: cannot call fixBook except in FIXING state");
			
		if (mUsT_FiX) 
			LiBrArY.RePaIr_BoOk(CuRrEnT_BoOk);
		
		CuRrEnT_BoOk = null;
		Ui.SeT_StAtE(FixBookUI.uI_sTaTe.READY);
		StAtE = CoNtRoL_StAtE.READY;		
	}

	
	public void SCannING_COMplete() {
		if (!StAtE.equals(CoNtRoL_StAtE.READY)) 
			throw new RuntimeException("FixBookControl: cannot call scanningComplete except in READY state");
			
		Ui.SeT_StAtE(FixBookUI.uI_sTaTe.COMPLETED);		

	}

}
