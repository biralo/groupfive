package library.borrowbook;
import java.util.Scanner;

public class BorrowBookUI {
    
	public static enum UiState { INITIALISED, READY, RESTRICTED, SCANNING, IDENTIFIED, FINALISING, COMPLETED, CANCELLED };
	private BorrowBookControl Control;
	private Scanner Input;
	private UiState State;
	
	public BorrowBookUI(BorrowBookControl control) {
		this.Control = control;
		Input = new Scanner(System.in);
		State = UiState.INITIALISED;
		control.Set_Ui(this);
	}

	private String Input(String Prompt) {
		System.out.print(Prompt);
		return Input.nextLine();
	}	
			
	private void Output(Object Object) {
		System.out.println(Object);
	}
			
	public void Set_State(UiState State) {
		this.State = State;
	}

	public void Run() {
		Output("Borrow Book Use Case UI\n");
		
		while (true) {
                        
			switch (State) {			
			
			case CANCELLED:
				Output("Borrowing Cancelled");
				return;
		
			case READY:
				String MEM_STR = Input("Swipe member card (press <enter> to cancel): ");
				if (MEM_STR.length() == 0) {
					Control.Cancel();
					break;
				}
				try {
					int memberId = Integer.valueOf(MEM_STR).intValue();
					Control.Swiped(memberId);
				}
				catch (NumberFormatException e) {
					Output("Invalid Member Id");
				}
				break;
	
			case RESTRICTED:
				Input("Press <any key> to cancel");
				Control.Cancel();
				break;
				
			case SCANNING:
				String Book_String_Input = Input("Scan Book (<enter> completes): ");
				if (Book_String_Input.length() == 0) {
					Control.Complete();
					break;
				}
				try {
					int bookId = Integer.valueOf(Book_String_Input).intValue();
					Control.Scanned(bookId);
					
				} catch (NumberFormatException e) {
					Output("Invalid Book Id");
				} 
				break;
						
			case FINALISING:
				String Ans = Input("Commit loans? (Y/N): ");
				if (Ans.toUpperCase().equals("N")) {
					Control.Cancel();
					
				} else {
					Control.Commit_Loans();
					Input("Press <any key> to complete ");
				}
				break;
					
			case COMPLETED:
				Output("Borrowing Completed");
				return;
	
				
			default:
				Output("Unhandled state");
				throw new RuntimeException("BorrowBookUI : unhandled state :" + State);			
			}
		}		
	}

	public void Display(Object object) {
		Output(object);		
	}
}
