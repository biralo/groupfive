package library.borrowbook;
import java.util.ArrayList;
import java.util.List;

import library.entities.Book;
import library.entities.Library;
import library.entities.Loan;
import library.entities.Member;

public class BorrowBookControl {
	
	private BorrowBookUi Ui;
	private Library Library;
	private Member Member;
	private enum ControlState { INITIALISED, READY, RESTRICTED, SCANNING, IDENTIFIED, FINALISING, COMPLETED, CANCELLED };
	private ControlState State;
	private List<Book> Pending_List;
	private List<Loan> Completed_List;
	private Book book;
	
	public BorrowBookControl() {
		this.Library = Library.getInstance();
		State = ControlState.INITIALISED;
	}

	public void Set_Ui(BorrowBookUi Ui) {
		if (!State.equals(ControlState.INITIALISED)) 
			throw new RuntimeException("BorrowBookControl: cannot call setUI except in INITIALISED state");
			
		this.Ui = Ui;
		Ui.Set_State(BorrowBookUi.Ui_State.READY);
		State = ControlState.READY;		
	}
		
	public void Swiped(int memberId) {
		if (!State.equals(ControlState.READY)) 
			throw new RuntimeException("BorrowBookControl: cannot call cardSwiped except in READY state");
			
		Member = Library.get_Member(memberId);
		if (Member == null) {
			Ui.Display("Invalid memberId");
			return;
		}
		if (Library.cAn_Member_BoRrOw(Member)) {
			Pending_List = new ArrayList<>();
			Ui.Set_State(BorrowBookUi.Ui_State.SCANNING);
			State = ControlState.SCANNING; 
		}
		else {
			Ui.Display("Member cannot borrow at this time");
			Ui.Set_State(BorrowBookUi.Ui_State.RESTRICTED); 
		}
	}
		
	public void Scanned(int bookId) {
		book = null;
		if (!State.equals(ControlState.SCANNING)) 
			throw new RuntimeException("BorrowBookControl: cannot call bookScanned except in SCANNING state");
			
		book = Library.get_Book(bookId);
		if (book == null) {
			Ui.Display("Invalid bookId");
			return;
		}
		if (!book.is_available()) {
			Ui.Display("Book cannot be borrowed");
			return;
		}
		Pending_List.add(book);
		for (Book B : Pending_List) 
			Ui.Display(B.toString());
		
		if (Library.get_number_of_loans_remaining_for_member(Member) - Pending_List.size() == 0) {
			Ui.Display("Loan limit reached");
			Complete();
		}
	}
	
	public void Complete() {
		if (Pending_List.size() == 0) 
			Cancel();
		
		else {
			Ui.Display("\nFinal Borrowing List");
			for (Book book : Pending_List) 
				Ui.Display(book.toString());
			
			Completed_List = new ArrayList<Loan>();
			Ui.Set_State(BorrowBookUi.Ui_State.FINALISING);
			State = ControlState.FINALISING;
		}
	}

	public void Commit_Loans() {
		if (!State.equals(ControlState.FINALISING)) 
			throw new RuntimeException("BorrowBookControl: cannot call commitLoans except in FINALISING state");
			
		for (Book B : Pending_List) {
			Loan loan = Library.issue_loan(B, Member);
			Completed_List.add(loan);			
		}
		Ui.Display("Completed Loan Slip");
		for (Loan loan : Completed_List) 
			Ui.Display(loan.toString());
		
		Ui.Set_State(BorrowBookUi.Ui_State.COMPLETED);
		State = ControlState.COMPLETED;
	}
	
	public void Cancel() {
		Ui.Set_State(BorrowBookUi.Ui_State.CANCELLED);
		State = ControlState.CANCELLED;
	}		
}
