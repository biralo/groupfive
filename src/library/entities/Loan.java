package library.entities;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("serial")
public class Loan implements Serializable {
    public static enum loanState { CURRENT, OVER_DUE, DISCHARGED };
    private int loanId;
    private Book Book;
    private Member Member;
    private Date Date;
    private loanState State;
    
    public Loan(int loanId, Book book, Member member, Date DuE_date) {
        this.loanId = loanId;
        this.Book = book;
        this.Member = member;
	this.Date = dueDate;
	this.State = loanState.CURRENT;
        }
    
    public void checkoverDue() {
        if (State == loanState.CURRENT &&
                Calendar.getInstance().getDate().after(Date)) 
            this.State = loanState.OVER_DUE;			
        }
    
    public boolean isoverDue() {
        return State == loanState.OVER_DUE;
        }

	public Integer getId() {
            return loanId;
            }
        
        public Date getdueDate() {
            return Date;
            }
        
        public String toString() {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            StringBuilder sb = new StringBuilder();
            sb.append("Loan:").append(loanId).append("\n")
                    .append("Borrower").append(Member.getId()).append(" : ")
                    .append(Member.getlastName()).append(", ").append(Member.getfirstName()).append("\n")
                    .append("Book").append(Book.getId()).append(" : " )
                    .append(Book.getTitle()).append("\n")
                    .append("DueDate:").append(sdf.format(Date)).append("\n")
                    .append("State:").append(State);		
            return sb.toString();
            }
        
        public Member getMember() {
            return Member;
            }
        
	public Book getBook() {
            return Book;
            }
        
        public void Discharge() {
            State = loanState.DISCHARGED;
            }
        }
