package library.entities;
import java.io.Serializable;

@SuppressWarnings("serial")
public class Book implements Serializable {
    
	private String Title;
	private String Author;
	private String callNo;
	private int Id;	
	private enum state { AVAILABLE, ON_LOAN, DAMAGED, RESERVED };
	private state State;
	
	public Book(String author, String title, String callNo, int id) {
		this.Author = author;
		this.Title = title;
		this.callNo = callNo;
		this.Id = id;
		this.State = state.AVAILABLE;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Book: ").append(Id).append("\n")
		  .append("  Title:  ").append(Title).append("\n")
		  .append("  Author: ").append(Author).append("\n")
		  .append("  CallNo: ").append(callNo).append("\n")
		  .append("  State:  ").append(State);
		
		return sb.toString();
	}

	public Integer getId() {
		return Id;
	}

	public String getTitle() {
		return Title;
	}
	
	public boolean is_Available() {
		return State == state.AVAILABLE;
	}

	public boolean is_on_Loan() {
		return State == state.ON_LOAN;
	}

	public boolean is_Damaged() {
		return State == state.DAMAGED;
	}
	
	public void Borrow() {
		if (State.equals(state.AVAILABLE)) 
			State = state.ON_LOAN;
		
		else 
			throw new RuntimeException(String.format("Book: cannot borrow while book is in state: %s", State));   	
	}

	public void Return(boolean Damaged) {
		if (State.equals(state.ON_LOAN)) 
			if (Damaged) 
				State = state.DAMAGED;
			
			else 
				State = state.AVAILABLE;
                
		else 
			throw new RuntimeException(String.format("Book: cannot Return while book is in state: %s", State));
				
	}
	
	public void Repair() {
		if (State.equals(state.DAMAGED)) 
			State = state.AVAILABLE;
		
		else 
			throw new RuntimeException(String.format("Book: cannot repair while book is in state: %s", State));
                
	}
        
}
