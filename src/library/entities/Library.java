package library.entities;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
public class Library implements Serializable {
	
	private static final String libraryFile = "library.obj";
	private static final int loanLimit = 2;
	private static final int loanPeriod = 2;
	private static final double finePerDay = 1.0;
	private static final double maxFinesOwed = 1.0;
	private static final double damageFee = 2.0;
	
	private static Library self;
	private int bookId;
	private int memberId;
	private int loanId;
	private Date loanDate;
        
	private Map<Integer, Book> Catalog;
	private Map<Integer, Member> Members;
	private Map<Integer, Loan> Loans;
	private Map<Integer, Loan> CurrentLoans;
	private Map<Integer, Book> DamagedBooks;
        
	private Library() {
            Catalog = new HashMap<>();
            Members = new HashMap<>();
            Loans = new HashMap<>();
            CurrentLoans = new HashMap<>();
            DamagedBooks = new HashMap<>();
            bookId = 1;
            memberId = 1;
            loanId = 1;	
            }

	public static synchronized Library getInstance() {
            if (self == null) {
                Path PATH = Paths.get(libraryFile);
                if (Files.exists(PATH)) {
                    try (ObjectInputStream Library_File = new ObjectInputStream(new FileInputStream(libraryFile));) {
                        self = (Library) Library_File.readObject();
                        Calendar.getInstance().setDate(self.loanDate);
                        Library_File.close();
                        }
                    catch (Exception e) {
                        throw new RuntimeException(e);
                        }
                    }
                else self = new Library();
                }
            return self;
            }

	public static synchronized void save() {
            if (self != null) {
                self.loanDate = Calendar.getInstance().getDate();
                try (ObjectOutputStream Library_File = new ObjectOutputStream(new FileOutputStream(libraryFile));) {
                    Library_File.writeObject(self);
                    Library_File.flush();
                    Library_File.close();
                    }
                catch (Exception e) {
                    throw new RuntimeException(e);
                    }
                }
            }

	public int getbookId() {
            return bookId;
	}
	
	public int getmember_Id() {
            return memberId;
	}
	
	private int getnextbookId() {
            return bookId;
	}

	private int getnextmemberId() {
            return memberId++;
	}

	private int getnextloanId() {
            return loanId++;
	}

	public List<Member> listMembers() {
            return new ArrayList<Member>(Members.values()); 
	}
        
	public List<Book> listBooks() {	
            return new ArrayList<Book>(Catalog.values()); 
	}
        
	public List<Loan> listcurrentLoans() {
            return new ArrayList<Loan>(CurrentLoans.values());
	}

	public Member addMember(String lastName, String firstName, String email, int phoneNo) {	
            Member member = new Member(lastName, firstName, email, phoneNo, getnextmemberId());
            Members.put(member.getId(), member);		
            return member;
	}
	
	public Book addBook(String a, String t, String c) {
            Book b = new Book(a, t, c, getnextbookId());
            Catalog.put(b.getId(), b);	
            return b;
	}

	public Member getmember(int memberId) {
            if (Members.containsKey(memberId)) 
                return Members.get(memberId);
            return null;
	}

	public Book getbook(int bookId) {
            if (Catalog.containsKey(bookId))
                return Catalog.get(bookId);
            return null;
	}

	public int getloanLimit() {
            return loanLimit;
	}

	public boolean canmemberBorrow(Member member) {		
		if (member.getnumberofcurrentLoans() == loanLimit )
                    return false;
				
		if (member.finesOwed() >= maxFinesOwed) 
                    return false;
				
		for (Loan loan : member.getLoans()) 
                    if (loan.Is_OvEr_DuE())
                        return false;
		return true;
	}
        
	public int getmumberofloansremainingforMember(Member member) {	
            return loanLimit - member.getnumberofcurrentLoans();
	}

	public Loan issueLoan(Book book, Member member) {
	    Date dueDate = Calendar.getInstance().getdueDate(loanPeriod);
	    Loan loan = new Loan(getnextloanId(), book, member, dueDate);
	    member.takeoutLoan(loan);
	    book.borrow();
	    Loans.put(loan.getId(), loan);
	    CurrentLoans.put(book.getId(), loan);
	    return loan;
	}
	
	public Loan getloanbybookId(int bookId) {
		if (CurrentLoans.containsKey(bookId)) 
		    return CurrentLoans.get(bookId);
		return null;
	}
        
	public double calculateoverdueFine(Loan loan) {
		if (Loan.is_over_Due()) {
		    long daysoverDue = Calendar.getInstance().getdaysDifference(Loan.getdueDate());
		    double fine = daysoverDue * finePerDay;
		    return fine;
		}
		return 0.0;		
	}

	public void dischargeLoan(Loan CurrentLoan, boolean is_damaged) {
		Member member = CurrentLoan.getMember();
		Book book  = CurrentLoan.getBook();
		
		double overdueFine = calculateoverdueFine(CurrentLoan);
		member.addFine(overdueFine);	
		member.dischargeLoan(CurrentLoan);
		book.Return(is_damaged);
                
		if (is_damaged) {
		    member.addFine(damageFee);
		    DamagedBooks.put(book.getId(), book);
		}
                
		CurrentLoan.Discharge();
		CurrentLoans.remove(book.getId());
	}

	public void checkcurrentLoans() {
		for (Loan loan : CurrentLoans.values())
                    
		    loan.checkoverDue();
				
	}

	public void repairbook(Book CurrentBook) {
		if (DamagedBooks.containsKey(CurrentBook.getId())) {
		    CurrentBook.Repair();
		    DamagedBooks.remove(CurrentBook.getId());
		}
		else
		    throw new RuntimeException("Library: repairBook: book is not damaged");	
	}
}
