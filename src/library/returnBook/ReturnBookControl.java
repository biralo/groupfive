package library.returnBook;
import library.entities.Book;
import library.entities.Library;
import library.entities.Loan;

public class ReturnBookControl {

	private ReturnBookUI Ui;
	private enum controlState { INITIALISED, READY, INSPECTING };
	private controlState state;

	private Library library;
	private Loan currentLoan;


	public ReturnBookControl() {
		this.library = Library.GeTiNsTaNcE();
		state = controlState.INITIALISED;
	}


	public void setUI(ReturnBookUI uI) {
		if (!state.equals(controlState.INITIALISED))
			throw new RuntimeException("ReturnBookControl: cannot call setUI except in INITIALISED state");

		this.Ui = uI;
		uI.setState(ReturnBookUI.uiState.READY);
		state = controlState.READY;
	}


	public void bookScanned(int bookId) {
		if (!state.equals(controlState.READY))
			throw new RuntimeException("ReturnBookControl: cannot call bookScanned except in READY state");

		Book currentBook = library.getbook(bookId);

		if (currentBook == null) {
			Ui.Display("Invalid Book Id");
			return;
		}
		if (!currentBook.iS_On_LoAn()) {
			Ui.Display("Book has not been borrowed");
			return;
		}
		currentLoan = library.GeT_LoAn_By_BoOkId(bookId);
		double Over_Due_Fine = 0.0;
		if (currentLoan.isoverDue())
			Over_Due_Fine = library.CaLcUlAtE_OvEr_DuE_FiNe(currentLoan);

		Ui.Display("Inspecting");
		Ui.Display(currentBook.toString());
		Ui.Display(currentLoan.toString());

		if (currentLoan.isoverDue())
			Ui.Display(String.format("\nOverdue fine : $%.2f", Over_Due_Fine));

		Ui.setState(ReturnBookUI.uiState.INSPECTING);
		state = controlState.INSPECTING;
	}


	public void scanningComplete() {
		if (!state.equals(controlState.READY))
			throw new RuntimeException("ReturnBookControl: cannot call scanningComplete except in READY state");

		Ui.setState(ReturnBookUI.uiState.COMPLETED);
	}


	public void dischargeLoan(boolean isDamaged) {
		if (!state.equals(controlState.INSPECTING))
			throw new RuntimeException("ReturnBookControl: cannot call dischargeLoan except in INSPECTING state");

		library.DiScHaRgE_LoAn(currentLoan, isDamaged);
		currentLoan = null;
		Ui.setState(ReturnBookUI.uiState.READY);
		state = controlState.READY;
	}


}

