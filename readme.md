# Welcome to GROUP 5 Repository

Greetings , This is the official repository for GROUP 5 students studying ITC205 at Charles Sturt University for the session 202060.

# Files

The codebase here is provided by Charles Sturt University and cannot be re-used or modified without the consent of the officials from **Charles Sturt University** 

The codebase is written in Java and will be constantly updated throughout the teaching period.


## Team Members for Group 5

 - Samip Pokharel
 - Bishant Neupane
 - Akash Banjade

## Communication Channel

Slack has been selected as the official communication channel for the group members. 

Please request to join the workspace : https://thegroupfive.slack.com (Channel : #university) 


## Repository Navigation Guidance

 - Each individual team member has a different branch which they will be using to make individual changes.
 - Iteration Plan, Team Charter, Meeting Minutes and further information on the communication channel can be found under "Team Documents" folder.
 - library contains the codebase.

**Note** : Please do not make a pull request unless you are a member of the team or an official from **Charles Sturt University**.
